﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebBrowser
{
    class Controller // Holds the main logic of the program. 
    {
        private GUI gui;
        private Data data;
        internal Controller(GUI gui, Data data)
        {
            this.gui = gui;
            this.data = data;

            //The ball starts rolling when a user clicks in the URL bar to type a URL... 
            gui.goButton.Click += new EventHandler(this.AfterGoClicked);// Add event handler for goButton click. 

            gui.backButton.Click += new EventHandler(this.AfterDirectionalClicked);
            gui.forwardButton.Click += new EventHandler(this.AfterDirectionalClicked);

            gui.homePage.Controls[1].Click += new EventHandler(gui.HomePageClicked);
            gui.homePage.Controls[0].Click += new EventHandler(gui.HomeEditClicked);
            gui.homePage.Paint += new PaintEventHandler(gui.HomePaint);

            gui.favourites.Controls[1].Click += new EventHandler(gui.FavouritesClicked);
            gui.favourites.Controls[0].Click += new EventHandler(gui.FavouritesAddClicked);
            gui.favourites.Paint += new PaintEventHandler(gui.HomePaint); // Able to re-use this method by passing in the correct sender. 

            gui.plusTabButton.Click += new EventHandler(this.PlusNewTab);

            gui.historyLabel.Click += new EventHandler(gui.HistoryClicked);
            gui.history.Paint += new PaintEventHandler(gui.HomePaint);    // Able to re-use this method by passing in the correct sender.       
        }

        internal void AfterGoClicked(object sender, EventArgs a)
        {

            // First put up a LOADING message. 
            gui.webContent.Text = "LOADING.....";

            if (data.TabList.Count == 0) // No tabs exist, so create one. 
            {
                this.PlusNewTab(null, null);
            }

            try
            {
                BackgroundWorker bgw = new BackgroundWorker();
                bgw.DoWork += new DoWorkEventHandler(DoBackgroundWork);
                bgw.RunWorkerAsync(a);
                //bw.RunWorkerAsync(a); // Runs the following method, "DoBackgroundWork"
            }
            catch (InvalidOperationException ioe)
            {
                Console.WriteLine(ioe.InnerException);
                MessageBox.Show("Please wait a moment, this one is taking a while...");
            }
        }

        internal void AfterDirectionalClicked(object sender, EventArgs e)
        {
            Button temp = (Button)sender;
            if (temp.Name == "forwards")
            {
                try
                {
                    TabHistoryEntry the = data.TabList[gui.SelectedTab].CurrentTabHistory.GetNext();
                    if (the == null)
                    {// Do nothing because there is no next entry. 
                    }
                    else
                    {
                        gui.urlBox.Text = the.OwnEntry.url;
                        data.TabList[gui.SelectedTab].CurrentTabHistory = the; // Set the currently-active history entry to be the one which was retrieved. 
                        BackgroundWorker directionalWorker = new BackgroundWorker(); // Now navigate to the retrieved URL. 
                        directionalWorker.DoWork += new DoWorkEventHandler(DoDirectionalBackgroundWork);
                        directionalWorker.RunWorkerAsync();
                    }
                }
                catch (NullReferenceException nre)
                {
                    Console.WriteLine(nre.InnerException);
                }

                
            }
            else if (temp.Name == "backwards")
            {
                TabHistoryEntry the = data.TabList[gui.SelectedTab].CurrentTabHistory.GetPrevious();
                if (the == null)
                {// Do nothing because there is no next entry. 
                }
                else
                {
                    gui.urlBox.Text = the.OwnEntry.url;
                    data.TabList[gui.SelectedTab].CurrentTabHistory = the; // Set the currently-active history entry to be the one which was retrieved. 
                    BackgroundWorker directionalWorker = new BackgroundWorker(); // Now navigate to the retrieved URL. 
                    directionalWorker.DoWork += new DoWorkEventHandler(DoDirectionalBackgroundWork);
                    directionalWorker.RunWorkerAsync();
                }
            }
        }

        // This is the method called by the background worker event handler. 
        internal void DoBackgroundWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            EventArgs a = (EventArgs)e.Argument;

            // Firstly alter URL if need to add http at the front
            gui.OnGoClick(sender, a);

            // Pass the URL to the currently-selected tab.
            string url = gui.urlBox.Text;
            data.TabList[gui.SelectedTab].TabURL = url;

            // Show the tab emoticon as loading. 
            data.TabList[gui.SelectedTab].emoticonLabel.Image = Properties.Resources.loading;

            // Add the URL to the overall history as well as the current tab's history
            data.history.AddHistoryItem(url);

            // If statement checks if the current entry is null or not, if null then add new one, otherwise begin the recursing. 
            if (data.TabList[gui.SelectedTab].CurrentTabHistory == null)
            {
                data.TabList[gui.SelectedTab].CurrentTabHistory = new TabHistoryEntry(DateTime.Now, url);
            }
            else
            {
                data.TabList[gui.SelectedTab].CurrentTabHistory.SetNextOrLast(new TabHistoryEntry(DateTime.Now, url));
                data.TabList[gui.SelectedTab].CurrentTabHistory = data.TabList[gui.SelectedTab].CurrentTabHistory.GetLast();
                //MessageBox.Show("previous is " + data.TabList[gui.SelectedTab].CurrentTabHistory.GetPrevious().OwnEntry.url);
            }

            /* Choose the currently-selected tab to use for the web request. 
             NB you want to do this AFTER adding to history, because otherwise if it takes a long time to load, and the user clicks
            on another tab, then the selected tab will be that one, and so the history will be added to it instead!!!! */
            data.TabList[gui.SelectedTab].GetWebContent();

            // Now tell GUI to show selected tab. 
            data.updateObservers(true);
        }

        internal void DoDirectionalBackgroundWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            EventArgs a = (EventArgs)e.Argument;

            // Firstly alter URL if need to add http at the front
            gui.OnGoClick(sender, a);

            // Pass the URL to the currently-selected tab.
            string url = gui.urlBox.Text;
            data.TabList[gui.SelectedTab].TabURL = url;

            // Show the tab emoticon as loading. 
            data.TabList[gui.SelectedTab].emoticonLabel.Image = Properties.Resources.loading;

            // Choose the currently-selected tab to use for the web request. 
            data.TabList[gui.SelectedTab].GetWebContent();

            // Miss out the step of adding to the overall history, as it's already been recorded there before. 
            //data.history.AddHistoryItem(url);

            // Now tell GUI to show selected tab. 
            data.updateObservers(true);
        }

        internal void PlusNewTab(object sender, EventArgs e)
        {
            string name = data.CreateNewTab();
            CreateTabEventHandlers(name);
            gui.SelectedTab = data.TabList[name].ID;
            data.updateObservers(true); // Which will update tab content, and will be blank for a new tab. 
        }

        internal void CreateTabEventHandlers(string id)
        {
            data.TabList[id].TabVar.Controls[1].MouseClick += new MouseEventHandler(gui.TabClick); // For clicking on tab, you want to display the web content. 
            data.TabList[id].TabVar.Controls[2].MouseClick += new MouseEventHandler(gui.TabClose); // For clicking on tab close, you want to close the tab. 
        }
    }
}
