﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebBrowser
{

    struct CacheData
    {
        internal DateTime dt;
        internal String html;
    }
    public class Data
    {

        internal List<GUI> observerList = new List<GUI>();
        internal Dictionary<string, Tab> tabs = new Dictionary<string, Tab>();
        private Dictionary<String, CacheData> cache = new Dictionary<String, CacheData>();

        internal Dictionary<String, CacheData> Cache
        {
            get { return this.cache; }
            set { this.cache = value; }
        }

        internal HomePage homePage;
        internal Favourites favourites;
        internal History history;

        internal Dictionary<string, Tab> TabList
        {
            get { return this.tabs; }
            set { this.tabs = value; }
        }
        public Data()
        {
            this.homePage = new HomePage();
            this.favourites = new Favourites();
            this.history = new History();

            try
            {
                this.homePage = (HomePage)this.InstantiateFromSerialised<HomePage>("home.xml");
                this.history = (History)this.InstantiateFromSerialised<History>("history.xml");
                this.favourites = (Favourites)this.InstantiateFromSerialised<Favourites>("favourites.xml");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.InnerException);
            }
            
        }

        internal void addGUIToObserverList(GUI gui)
        {
            this.observerList.Add(gui);
        }

        internal void CacheAdd(String URL, String HTML)
        {
            CacheData temp = new CacheData();
            temp.dt = DateTime.Now;
            temp.html = HTML;
            this.cache[URL] = temp; // Replaces the existing value with this updated version with the latest DateTime. 
        }

        internal bool CacheContainsCheck(String URL)
        {
            if(this.cache.ContainsKey(URL))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal bool CacheNotTimedOut(String URL)
        {
            int timedifference = 300;

            var vartimedifference = (DateTime.Now - this.cache[URL].dt).TotalSeconds;
            timedifference = (int)vartimedifference;
            // If cached URL was requested before less than 5 mins ago then return the cached entry. 
            if(timedifference < 300)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        internal String CacheGet(String URL)
        {
            return this.cache[URL].html;
        }
        internal string CreateNewTab()
        {
            string name = RandomString(20, true); ; // Name will be random string. 
            Tab newtab = new Tab(name,this);

            tabs.Add(name, newtab);
            /*tabs[name].ID = name;
            tabs[name].TabVar.Controls[0].Name = name;
            tabs[name].TabVar.Controls[1].Name = name;*/

            this.updateObservers(false);// False because don't want the lite update, want the full update to see the new set of tabs.
            return name; // Return the ID. 
        }

        // Taken from http://timtrott.co.uk/random-strings-generator/ 
        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 1; i < size + 1; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            else
                return builder.ToString();
        }

        internal void RemoveTab(string idname)
        {
            try
            {

                tabs.Remove(idname);
                this.updateObservers(false); // False because don't want the lite update, want the full update to see the new set of tabs.
            }
            catch (ArgumentOutOfRangeException aoore)
            {
                Console.WriteLine(aoore.Message);

            }
        }

        internal void updateObservers(bool lite)
        {
            foreach (GUI gui in observerList)
            {
                gui.updateData(lite);
            }
        }

        internal void DoSerialisation<T>(T serializableObject, string fileName)
        {
            try
            {
                // Credits to http://stackoverflow.com/questions/6115721/how-to-save-restore-serializable-object-to-from-file 
                System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
                System.Xml.Serialization.XmlSerializer xserialiser = new System.Xml.Serialization.XmlSerializer(serializableObject.GetType());
                using (System.IO.MemoryStream mstream = new System.IO.MemoryStream())
                {
                    xserialiser.Serialize(mstream, serializableObject);
                    mstream.Position = 0;
                    xdoc.Load(mstream);
                    xdoc.Save(fileName);
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.InnerException.Message);
            }
        }

        internal T InstantiateFromSerialised<T>(string fileName)
        {
            T returner = default(T);
            // Credits to http://stackoverflow.com/questions/6115721/how-to-save-restore-serializable-object-to-from-file 
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            xdoc.Load(fileName); // Loads as xml. 

            System.Xml.Serialization.XmlSerializer xdeserialiser = new System.Xml.Serialization.XmlSerializer(typeof(T)); // Create an xml deserializer for the correct type
            using (System.IO.StringReader reader = new System.IO.StringReader(xdoc.OuterXml)) // Creates new IO stringreader instance and passes in the xml string
            {
                using (System.Xml.XmlReader xreader = new System.Xml.XmlTextReader(reader)) // Creates new xmlreader instance passing the stringreader instance
                {
                    returner = (T)xdeserialiser.Deserialize(xreader); // Calls deserialise on the xmlreader
                    xreader.Close();
                    return returner; // Returns the deserialsed object. 
                }
            }
           
        }

    }
}
