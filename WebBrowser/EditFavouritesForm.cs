﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebBrowser
{
    public partial class EditFavouritesForm : Form
    {
        private Data data;
        private GUI gui;

        private FlowLayoutPanel favouritesPanel;

        public EditFavouritesForm(bool addNewFavourite, string URL, Data data, GUI gui)
        {
            
            this.data = data;
            this.gui = gui;
            InitializeComponent();
            this.Text = "Favourites";

            this.BackColor = Color.DarkGray;
            this.Size = new Size(320,500);
            //this.Show(); // Need this to update the width and height of form.
            int formwidth = this.Width;
            int formheight = this.Height;

            favouritesPanel = new FlowLayoutPanel();
            favouritesPanel.BackColor = Color.Black;
            favouritesPanel.Size = new Size(formwidth-20, formheight-39);
            favouritesPanel.AutoScroll = true;

            if (addNewFavourite)
            {
                FlowLayoutPanel AdderPanel = new FlowLayoutPanel();
                AdderPanel.Name = "adderPanel";
                AdderPanel.Size = new Size(200, 200);

                Label urlheader = new Label();
                urlheader.Size = new Size(195, 30);
                urlheader.Text = "ADD THIS PAGE TO FAVOURITES: ";
                urlheader.BackColor = Color.Black;
                urlheader.ForeColor = Color.Green;
                TextBox favouritesURL = new TextBox();
                favouritesURL.Name = "NewURL";
                favouritesURL.BackColor = Color.Black;
                favouritesURL.ForeColor = Color.Green;
                favouritesURL.Size = new Size(195, 30);
                favouritesURL.Multiline = false;
                favouritesURL.Text = URL;

                Label nameheader = new Label();
                nameheader.Size = new Size(195, 30);
                nameheader.Text = "name you want to give this page";
                nameheader.BackColor = Color.Black;
                nameheader.ForeColor = Color.Green;
                TextBox favouriteName = new TextBox();
                favouriteName.Name = "newName";
                favouriteName.BackColor = Color.Black;
                favouriteName.ForeColor = Color.Green;
                favouriteName.Size = new Size(195, 30);
                favouriteName.Multiline = false;

                Button saveNewFavourite = new Button();
                favouriteName.Size = new Size(60, 30);
                saveNewFavourite.Text = "Add";
                saveNewFavourite.Click += new EventHandler(AddSave);
                AdderPanel.Controls.Add(urlheader);
                AdderPanel.Controls.Add(favouritesURL);
                AdderPanel.Controls.Add(nameheader);
                AdderPanel.Controls.Add(favouriteName);
                AdderPanel.Controls.Add(saveNewFavourite);
                this.Controls.Add(AdderPanel);
            }

            else
            {
                RefreshSetup();
            }
        }

        internal void RefreshSetup()
        {
            foreach (StringsKeyValuePair skvp in data.favourites.FavouritesList)
            {
                /*Label urlheader = new Label();
                urlheader.Size = new Size(60, 30);
                urlheader.Text = "Page URL";
                urlheader.BackColor = Color.Black;
                urlheader.ForeColor = Color.Green;*/

                TextBox favouritesURL = new TextBox();
                favouritesURL.BackColor = Color.Black;
                favouritesURL.ForeColor = Color.Green;
                favouritesURL.Size = new Size(195, 30);
                favouritesURL.Multiline = false;
                favouritesURL.Text = skvp.Key;
                favouritesURL.Name = "key";

                Button visitThis = new Button();
                visitThis.Size = new Size(50, 30);
                visitThis.BackColor = Color.DarkGray;
                visitThis.Name = favouritesURL.Text;
                visitThis.Click += new EventHandler(VisitNow);
                visitThis.Text = "Visit!";

                Label nameheader = new Label();
                nameheader.Size = new Size(50, 30);
                nameheader.Text = "Name";
                nameheader.BackColor = Color.Black;
                nameheader.ForeColor = Color.Green;

                Button deleteThis = new Button(); 
                deleteThis.Name = skvp.Key;
                deleteThis.Size = new Size(30, 30);
                deleteThis.Image = Properties.Resources.close;
                deleteThis.Click += new EventHandler(this.DeleteEntry);
                deleteThis.Cursor = Cursors.Hand;

                TextBox favouriteName = new TextBox();
                favouriteName.BackColor = Color.Black;
                favouriteName.ForeColor = Color.Green;
                favouriteName.Size = new Size(195, 30);
                favouriteName.Multiline = false;
                favouriteName.Text = skvp.Value;
                favouriteName.Name = "value";

                Label separator = new Label();
                separator.AutoSize = false;
                separator.Height = 2;
                separator.Width = this.Width - 20;
                separator.BorderStyle = BorderStyle.Fixed3D;
                separator.BackColor = Color.Green;
                separator.ForeColor = Color.Green;

                favouritesPanel.Controls.Add(visitThis);
                favouritesPanel.Controls.Add(favouritesURL);
                favouritesPanel.Controls.Add(deleteThis);
                favouritesPanel.Controls.Add(nameheader);
                favouritesPanel.Controls.Add(favouriteName);
                favouritesPanel.Controls.Add(separator);
            }

            Button favouritesSave = new Button();
            favouritesSave.BackColor = Color.Gray;
            favouritesSave.ForeColor = Color.Black;
            favouritesSave.Size = new Size(150, 40);
            favouritesSave.Text = "Close (current data will be saved)";
            favouritesSave.Click += new EventHandler(OnSave);
            favouritesPanel.Controls.Add(favouritesSave);
            this.Controls.Add(favouritesPanel);
            favouritesPanel.Refresh();
        }

        internal void OnSave(object sender, EventArgs e)
        {
            Control[] keyarray = this.Controls.Find("key", true);
            Control[] valuearray = this.Controls.Find("value", true);
            data.favourites.FavouritesList.Clear(); // Delete the contents of the favourites dictionary. 
            for (int x = 0; x < keyarray.Length; x++)
            {           
                data.favourites.AddPageToFavourites(keyarray[x].Text, valuearray[x].Text); // Refresh the contents from the controls here, which will catch any changes the user made. 
            }

            this.Close();
            this.Dispose();
        }

        internal void AddSave(object sender, EventArgs e)
        {
            Control[] carray = this.Controls.Find("NewURL",true);
            
            Control[] narray = this.Controls.Find("newName", true);
            bool success = data.favourites.AddPageToFavourites(carray[0].Text, narray[0].Text);
            if (!success)
            {
                MessageBox.Show("This URL already exists... unable to add it again");
            }

            Control[] aarray = this.Controls.Find("adderPanel", true);
            aarray[0].Controls.Clear();
            aarray[0].Refresh();
            aarray[0].Dispose();

            RefreshSetup();
        }

        private void VisitNow(object sender, EventArgs e)
        {
            Button thesender = (Button)sender;
            
            gui.urlBox.Text = thesender.Name;
            gui.goButton.PerformClick();
            this.Close();
            this.Dispose();
        }

        private void DeleteEntry(object sender, EventArgs e)
        {
            Button temp = (Button)sender;
            string name = temp.Name;
            bool deleted = data.favourites.FavouritesList.DeleteByKey(name);
            favouritesPanel.Controls.Clear();
            this.RefreshSetup();
        }
    }
}
