﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebBrowser
{
    public partial class EditHomeForm : Form
    {
        private Data data;

        private FlowLayoutPanel homepagePanel;
        private Label homePageHeader;
        private TextBox homePageURL;
        private Button homePageSave;
        public EditHomeForm(Data data)
        {
            
            this.data = data;
            InitializeComponent();
            this.Text = "Home Page";

            this.BackColor = Color.DarkGray;
            this.Size = new Size(215, 120);

            homepagePanel = new FlowLayoutPanel();
            homepagePanel.BackColor = Color.Black;

            homePageHeader = new Label();
            homePageHeader.Text = "Home Page URL";
            homePageHeader.BackColor = Color.Black;
            homePageHeader.ForeColor = Color.Green;
            homePageURL = new TextBox();
            homePageURL.BackColor = Color.Black;
            homePageURL.ForeColor = Color.Green;
            homePageURL.Size = new Size(195, 30);
            homePageURL.Multiline = false;
            // Display the current URL, or lack of. 
            if(data.homePage.URL == "" || data.homePage.URL == null)
            {
                homePageURL.Text = "Enter a URL here";
            }
            else
            {
                homePageURL.Text = data.homePage.URL;
            }
            homePageSave = new Button();
            homePageSave.BackColor = Color.Gray;
            homePageSave.ForeColor = Color.Black;
            homePageSave.Size = new Size(100, 30);
            homePageSave.Text = "Save home page URL";
            homePageSave.Click += new EventHandler(OnSave);
            homepagePanel.Controls.Add(homePageHeader);
            homepagePanel.Controls.Add(homePageURL);
            homepagePanel.Controls.Add(homePageSave);
            this.Controls.Add(homepagePanel);
        }

        internal void OnSave(object sender, EventArgs e)
        {
            data.homePage.URL = homePageURL.Text;
            data.DoSerialisation(data.homePage,"home.xml"); // Saves homepage to file. 
            this.Close();
            this.Dispose();
        }



    }
}
