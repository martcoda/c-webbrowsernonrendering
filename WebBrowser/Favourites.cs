﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBrowser
{
    [Serializable]
    public class Favourites
    {
        internal ReflectableDictionary favouritesList = new ReflectableDictionary(); // url and name

        public ReflectableDictionary FavouritesList
        {
            get { return this.favouritesList; }
        }

        internal bool AddPageToFavourites(string url, string nameForIt)
        {
           
            if(this.favouritesList.ContainsKey(url))
            {
                return false;
            }
            else
            {
                StringsKeyValuePair temp = new StringsKeyValuePair();
                temp.Key = url;
                temp.Value = nameForIt;
                this.favouritesList.Add(temp);
                return true;
            }
        }
    }
}
