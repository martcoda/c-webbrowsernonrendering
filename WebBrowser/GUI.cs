﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebBrowser
{
    public partial class GUI : Form
    {
        private Data data;
        internal FlowLayoutPanel menuPanel;
        internal FlowLayoutPanel homePage;
        internal Label editHome;
        internal Label homeLabel;

        internal FlowLayoutPanel favourites;
        internal FlowLayoutPanel history;
        internal Label historyLabel;

        internal Button backButton;
        internal Button forwardButton;
        internal Button plusTabButton;
        internal TextBox urlBox;
        internal Boolean urlFirstClick = true; //starts as true because when u click on url will be first time
        internal Button goButton;
        internal FlowLayoutPanel tabPanel;

        internal Label favouritesLabel;
        internal Label addfavourites;

        internal RichTextBox webContent;

        private string selectedTab = "";

        internal string SelectedTab
        {
            get { return this.selectedTab; }
            set { this.selectedTab = value; }
        }

        internal GUI(Data data)
        {

            this.data = data;
            data.addGUIToObserverList(this); // Add this form GUI to the list of observers. 
            InitializeComponent();
            this.Text = "MK Web Browser";
            this.BackColor = Color.Black;
            this.WindowState = FormWindowState.Maximized; // Set form size to fill monitor
            this.Show(); // Need this to update the width and height of form.
            int formwidth = this.Width;
            int formheight = this.Height - 230;

            // Menu bar down the side
            menuPanel = new FlowLayoutPanel();
            menuPanel.BackColor = Color.Black;
            menuPanel.ForeColor = Color.Green;
            menuPanel.Location = new System.Drawing.Point(9, 200);
            menuPanel.Size = new Size(70, formheight - 49); // Set urlbox to 50 less than width of page.

            // Home page section in the menu bar
            homePage = new FlowLayoutPanel();
            homePage.Size = new System.Drawing.Size(59, 67);
            homePage.Location = new System.Drawing.Point(9, 9);
            homePage.BorderStyle = BorderStyle.FixedSingle;

            Bitmap image = new Bitmap(Properties.Resources.edit);
            Bitmap sizedImage = new Bitmap(image, 15, 15);
            editHome = new Label();
            editHome.Name = "EditHome";
            editHome.Image = sizedImage;
            editHome.Size = new Size(20, 14);
            editHome.Cursor = Cursors.Hand;
            ToolTip editHometoolTip = new ToolTip();
            editHometoolTip.AutoPopDelay = 5000;
            editHometoolTip.InitialDelay = 200;
            editHometoolTip.ReshowDelay = 200;
            editHometoolTip.ShowAlways = true;
            editHometoolTip.SetToolTip(this.editHome, "Edit Homepage");

            homeLabel = new Label();
            homeLabel.Size = new Size(47, 50);
            homeLabel.Image = Properties.Resources.home;
            homeLabel.Cursor = Cursors.Hand;
            ToolTip HomeLabeltoolTip = new ToolTip();
            HomeLabeltoolTip.AutoPopDelay = 5000;
            HomeLabeltoolTip.InitialDelay = 200;
            HomeLabeltoolTip.ReshowDelay = 200;
            HomeLabeltoolTip.ShowAlways = true;
            HomeLabeltoolTip.SetToolTip(this.homeLabel, "Visit Homepage");

            homePage.Controls.Add(editHome);
            homePage.Controls.Add(homeLabel);

            this.Controls.Add(homePage);

            // Favourites menu item
            favourites = new FlowLayoutPanel();
            favourites.Size = new System.Drawing.Size(59, 79);
            favourites.BorderStyle = BorderStyle.FixedSingle;

            addfavourites = new Label();
            addfavourites.Name = "EditFavourites";
            addfavourites.Image = Properties.Resources.add;
            addfavourites.Size = new Size(25, 25);
            addfavourites.Cursor = Cursors.Hand;
            ToolTip addfavouritestoolTip = new ToolTip();
            addfavouritestoolTip.AutoPopDelay = 5000;
            addfavouritestoolTip.InitialDelay = 200;
            addfavouritestoolTip.ReshowDelay = 200;
            addfavouritestoolTip.ShowAlways = true;
            addfavouritestoolTip.SetToolTip(this.addfavourites, "Add Current Website to Favourites");

            favouritesLabel = new Label();
            favouritesLabel.Size = new Size(47, 47);
            favouritesLabel.Image = Properties.Resources.favourites;
            favouritesLabel.Cursor = Cursors.Hand;
            ToolTip favouritestoolTip = new ToolTip();
            favouritestoolTip.AutoPopDelay = 5000;
            favouritestoolTip.InitialDelay = 200;
            favouritestoolTip.ReshowDelay = 200;
            favouritestoolTip.ShowAlways = true;
            favouritestoolTip.SetToolTip(this.favouritesLabel, "View Favourites");

            favourites.Controls.Add(addfavourites);
            favourites.Controls.Add(favouritesLabel);

            //History menu item
            history = new FlowLayoutPanel();

            history.Size = new System.Drawing.Size(59, 79);
            history.BorderStyle = BorderStyle.FixedSingle;

            Label historySpacer = new Label();
            historySpacer.Image = Properties.Resources.history_spacer;
            historySpacer.Size = new Size(10, 10);

            historyLabel = new Label();
            historyLabel.Name = "History";
            historyLabel.Image = Properties.Resources.history;
            historyLabel.Size = new Size(40, 50);
            historyLabel.Cursor = Cursors.Hand;
            ToolTip addhistorytoolTip = new ToolTip();
            addhistorytoolTip.AutoPopDelay = 5000;
            addhistorytoolTip.InitialDelay = 200;
            addhistorytoolTip.ReshowDelay = 200;
            addhistorytoolTip.ShowAlways = true;
            addhistorytoolTip.SetToolTip(this.historyLabel, "View History");

            history.Controls.Add(historySpacer);
            history.Controls.Add(historyLabel);

            //menuPanel.Controls.Add(homePage);
            menuPanel.Controls.Add(favourites);
            menuPanel.Controls.Add(history);
            Controls.Add(menuPanel);

            // Back button
            backButton = new Button();
            backButton.Cursor = Cursors.Hand;
            backButton.Text = "Back";
            backButton.Name = "backwards";
            backButton.Location = new System.Drawing.Point(79, 9);
            backButton.Size = new Size(59, 64);
            Image backImage = new Bitmap((Properties.Resources.backward));
            Image newbackimage = new Bitmap(backImage, 47, 49);
            backButton.BackgroundImage = newbackimage;
            backButton.BackgroundImageLayout = ImageLayout.Stretch;
            ToolTip backwardButtontoolTip = new ToolTip();
            backwardButtontoolTip.AutoPopDelay = 5000;
            backwardButtontoolTip.InitialDelay = 200;
            backwardButtontoolTip.ReshowDelay = 200;
            backwardButtontoolTip.ShowAlways = true;
            backwardButtontoolTip.SetToolTip(this.backButton, "Backward");
            Controls.Add(backButton);

            // Forward button
            forwardButton = new Button();

            forwardButton.Cursor = Cursors.Hand;
            forwardButton.Text = "Forward";
            forwardButton.Name = "forwards";
            forwardButton.Location = new System.Drawing.Point(148, 9);
            forwardButton.Size = new Size(59, 64);
            Image forwardImage = new Bitmap((Properties.Resources.forward));
            Image newforwardimage = new Bitmap(forwardImage, 47, 49);
            forwardButton.BackgroundImage = forwardImage;
            forwardButton.BackgroundImageLayout = ImageLayout.Stretch;
            ToolTip forwardButtontoolTip = new ToolTip();
            forwardButtontoolTip.AutoPopDelay = 5000;
            forwardButtontoolTip.InitialDelay = 200;
            forwardButtontoolTip.ReshowDelay = 200;
            forwardButtontoolTip.ShowAlways = true;
            forwardButtontoolTip.SetToolTip(this.forwardButton, "Forward");
            Controls.Add(forwardButton);

            //URL box
            urlBox = new TextBox();
            urlBox.Font = new Font(urlBox.Font.FontFamily, 16);
            urlBox.Multiline = true;
            urlBox.BackColor = Color.Black;
            urlBox.ForeColor = Color.Green;
            urlBox.Text = "Enter URL here";
            urlBox.Multiline = false;
            urlBox.Location = new System.Drawing.Point(79, 84);
            urlBox.Size = new Size(formwidth - 119, 59); // Set urlbox to 50 less than width of page. 
            urlBox.MouseClick += new MouseEventHandler(urlClick);
            Controls.Add(urlBox);

            // Go button
            goButton = new Button();
            goButton.Cursor = Cursors.Hand;
            goButton.Text = "Go";
            goButton.Location = new System.Drawing.Point(9, 84);
            goButton.Size = new Size(64, 49); // Set urlbox to 50 less than width of page. 
            Image goImage = new Bitmap((Properties.Resources.go));
            Image newgoimage = new Bitmap(goImage, 47, 49);
            goButton.BackgroundImage = newgoimage;
            goButton.BackgroundImageLayout = ImageLayout.Stretch;
            ToolTip goButtontoolTip = new ToolTip();
            goButtontoolTip.AutoPopDelay = 5000;
            goButtontoolTip.InitialDelay = 200;
            goButtontoolTip.ReshowDelay = 200;
            goButtontoolTip.ShowAlways = true;
            goButtontoolTip.SetToolTip(this.goButton, "Go");
            Controls.Add(goButton);

            // Plus tab button. 
            plusTabButton = new Button();
            plusTabButton.Cursor = Cursors.Hand;
            plusTabButton.Location = new System.Drawing.Point(20, 136);
            plusTabButton.Size = new Size(39, 39); // Set urlbox to 50 less than width of page. 
            Image plusImage = new Bitmap((Properties.Resources.plus));
            Image newPlusImage = new Bitmap(plusImage, 47, 49);
            plusTabButton.BackgroundImage = newPlusImage;
            plusTabButton.BackgroundImageLayout = ImageLayout.Stretch;
            ToolTip plusButtontoolTip = new ToolTip();
            plusButtontoolTip.AutoPopDelay = 5000;
            plusButtontoolTip.InitialDelay = 200;
            plusButtontoolTip.ReshowDelay = 200;
            plusButtontoolTip.ShowAlways = true;
            plusButtontoolTip.SetToolTip(this.plusTabButton, "Add new tab");
            Controls.Add(plusTabButton);

            // Tab area. 
            tabPanel = new FlowLayoutPanel();
            tabPanel.Location = new System.Drawing.Point(79, 118);
            tabPanel.Size = new Size(formwidth - 119, 74);
            tabPanel.AutoScroll = true;
            tabPanel.BackColor = Color.Black;
            tabPanel.WrapContents = true;
            Controls.Add(tabPanel);

            // Webcontent area. 
            webContent = new RichTextBox();
            webContent.Location = new System.Drawing.Point(79, 193);
            webContent.Size = new Size(formwidth - 118, formheight - 10);
            webContent.BackColor = Color.Black;
            webContent.ForeColor = Color.Green;
            webContent.Multiline = true;
            webContent.ScrollBars = (System.Windows.Forms.RichTextBoxScrollBars)ScrollBars.Vertical;
            Controls.Add(webContent);

            //Create intitial tab. 

        }

        // Function for the MVC pattern, updates the GUI when called. 
        internal void updateData(bool lite)
        {
            if (lite) // If the lite version of updateObservers, just do the showtabwebcontent. 
            {
                this.ShowTabWebContent();
            }

            else
            {


                if (tabPanel.InvokeRequired)
                {
                    this.tabPanel.Invoke((MethodInvoker)(() =>
                    {
                        tabPanel.Controls.Clear(); // First remove all existing tabs.
                    }));

                }
                else
                {
                    tabPanel.Controls.Clear(); // First remove all existing tabs. 
                }



                if (data.TabList.Count == 0)
                {
                    this.webContent.Text = "";
                }
                else
                {
                    bool selected = false;
                    foreach (Tab t in data.TabList.Values)
                    {
                        if (tabPanel.InvokeRequired)
                        {
                            this.tabPanel.Invoke((MethodInvoker)(() =>
                            {
                                tabPanel.Controls.Add(t.TabVar);
                            }));
                        }
                        else
                        {
                            tabPanel.Controls.Add(t.TabVar);
                        }

                        FlowLayoutPanel tab = new FlowLayoutPanel();
                        tab = t.TabVar;
                        tab.Paint += new PaintEventHandler(tabPanel_Paint); // Use Paint eventhandler to set border style. 
                        if (this.selectedTab == t.ID)
                        {
                            selected = true;
                        }
                    }

                    if (!selected)
                    { // Make the first tab the selected one, after a tab is removed. 
                        this.selectedTab = data.TabList.Values.ElementAt(0).ID;
                    }

                    this.ShowTabWebContent();
                }
            }
        }

        // Function to display the HTML received in the GUI. 
        internal void ShowTabWebContent()
        {
            string webContent = "";
            foreach (Tab tab in data.TabList.Values)
            {
                if (tab.TabVar.Controls[1].Name == this.selectedTab) // Try to find which tab is being closed. 
                {
                    webContent = tab.TabHTML;
                    if (this.webContent.InvokeRequired)
                    {
                        this.webContent.Invoke((MethodInvoker)(() =>
                        {
                            this.webContent.Text = webContent;
                        }));
                    }
                    else
                    {
                        this.webContent.Text = webContent;
                    }

                    return;
                }
            }

        }


        //Event handler methods:

        //Paint event handler for your Panel
        //thankyou http://stackoverflow.com/questions/19163809/set-panel-border-thickness-in-c-sharp-winform
        private void tabPanel_Paint(object sender, PaintEventArgs e)
        {
            //Console.WriteLine(sender.GetType());
            FlowLayoutPanel thePanel = (FlowLayoutPanel)sender;

            if (thePanel.BorderStyle == BorderStyle.FixedSingle)
            {
                int thickness = 3;//it's up to you
                int halfThickness = thickness / 2;
                Color color;

                if (thePanel.Name == this.selectedTab)
                {
                    color = Color.Red;
                }
                else
                {
                    color = Color.Green;
                }
                using (Pen p = new Pen(color, thickness))
                {
                    e.Graphics.DrawRectangle(p, new Rectangle(halfThickness,
                                                              halfThickness,
                                                              thePanel.ClientSize.Width - thickness,
                                                              thePanel.ClientSize.Height - thickness));
                }
            }
        }

        // Handles the paint event for menu items on the GUI, the point is to add a border, because C# doesn't have an easier way of doing this. 
        internal void HomePaint(object sender, PaintEventArgs e)
        {

            FlowLayoutPanel theControl = (FlowLayoutPanel)sender;


            if (theControl.BorderStyle == BorderStyle.FixedSingle)
            {
                int thickness = 3;//it's up to you
                int halfThickness = thickness / 2;
                Color color = Color.Green;
                using (Pen p = new Pen(color, thickness))
                {
                    e.Graphics.DrawRectangle(p, new Rectangle(halfThickness,
                                                              halfThickness,
                                                              theControl.ClientSize.Width - thickness,
                                                              theControl.ClientSize.Height - thickness));
                }
            }
        }

        internal void OnGoClick(object sender, EventArgs e)
        {
            // First check if http is at the front of the url, if not add it there. 
            string firstseven;
            try
            {
                firstseven = urlBox.Text.Substring(0, 7);
            }
            catch (ArgumentOutOfRangeException aore)
            {
                Console.WriteLine(aore.Message);
                firstseven = urlBox.Text;// Just take full string if it's too short for 8 chars. 
            }
            string comparer = "http://";
            if (!firstseven.Equals(comparer))
            {
                if (urlBox.InvokeRequired)
                {
                    urlBox.Invoke((MethodInvoker)(() =>
                    {
                        urlBox.Text = comparer + urlBox.Text; ;
                    }));
                }
                else
                {
                    urlBox.Text = comparer + urlBox.Text;
                }
            }
        }

        internal void HomePageClicked(object sender, EventArgs e)
        {
            if (data.homePage.URL != "" && data.homePage.URL != null)
            {
                this.urlBox.Text = data.homePage.URL;
                this.goButton.PerformClick();
            }

            else
            {
                MessageBox.Show("You need to create a homepage first, use the edit button (the spanner)");
            }
        }

        internal void HomeEditClicked(object sender, EventArgs e)
        {
            EditHomeForm editform = new EditHomeForm(this.data);
            editform.ShowDialog(); // Run the Windows Form. 
        }

        internal void FavouritesClicked(object sender, EventArgs e)
        {

            EditFavouritesForm efform = new EditFavouritesForm(false, "", this.data, this);
            efform.ShowDialog();

        }

        internal void FavouritesAddClicked(object sender, EventArgs e)
        {
            try
            {
                EditFavouritesForm efform = new EditFavouritesForm(true, data.TabList[this.selectedTab].TabURL, this.data, this);
                efform.ShowDialog(); // Show the form. 
            }
            catch (KeyNotFoundException knf)
            {
                Console.WriteLine(knf.InnerException);
                MessageBox.Show("Sorry no tab is active with a web page");
            }

        }

        internal void HistoryClicked(object sender, EventArgs e)
        {
            BackgroundWorker historybgw = new BackgroundWorker();
            historybgw.DoWork += new DoWorkEventHandler(DoHistory);
            historybgw.RunWorkerAsync();
        }

        internal void DoHistory(object sender, DoWorkEventArgs e)
        {
            HistoryForm histform = new HistoryForm(this, this.data);
            histform.ShowDialog(); // Show the form. 
        }

        private void urlClick(object sender, MouseEventArgs e)
        {
            if (urlFirstClick)
            {
                this.urlBox.Text = "";
                urlFirstClick = false;
            }
        }

        internal void TabClick(object sender, MouseEventArgs e)
        {
            if (this.webContent.InvokeRequired)
            {
                this.webContent.Invoke((MethodInvoker)(() =>
                {
                    this.webContent.Text = "";
                }));
            }
            else
            {
                this.webContent.Text = ""; // Clear out current contents. 
            }
            this.webContent.Refresh();
            TextBox temp = (TextBox)sender;
            this.selectedTab = temp.Name;


            // Re-draw the tabs to show the selected red border
            foreach (Tab t in data.TabList.Values)
            {
                t.TabVar.Invalidate();
                t.TabVar.Update();
            }

            this.ShowTabWebContent();
        }

        internal void TabClose(object sender, MouseEventArgs e)
        {
            /*System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace();
            System.Reflection.MethodBase methodbase = trace.GetFrame(1).GetMethod();
            Console.WriteLine("was called by " + methodbase.Name);*/
            Label temp = (Label)sender;

            string idname = "";
            foreach (Tab tab in data.TabList.Values)
            {
                if (tab.TabVar.Controls[1].Name == temp.Name) // Try to find which tab is being closed. 
                {
                    idname = temp.Name;
                    goto endofloop;
                }
            }

        endofloop: data.RemoveTab(idname); // Need to do this outside of the foreach
        }





    }
}