﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBrowser
{
    public struct HistoryEntry
    {
        public DateTime datetime;
        public String url;

        public HistoryEntry(DateTime datetime, String url)
        {
            this.datetime = datetime;
            this.url = url;
        }
        
    }
    [Serializable]
    public class History : ICloneable
    {
        

        internal List<HistoryEntry> historyList = new List<HistoryEntry>();

        public List<HistoryEntry> HistoryList
        {
            get { return this.historyList; }
        }

        internal void AddHistoryItem(String url)
        {
            HistoryEntry temp = new HistoryEntry();
            temp.datetime = DateTime.Now;
            temp.url = url;
            this.historyList.Add(temp); // Add the new entry. 
            if (historyList.Count > 20)
            {
                historyList.RemoveAt(0); // Remove the earliest entry if the list gets too big. 
            }

        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }



    }
}
