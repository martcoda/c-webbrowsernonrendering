﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebBrowser
{
    public partial class HistoryForm : Form
    {
        private Data data;
        private GUI gui;
        private FlowLayoutPanel initial;
        private FlowLayoutPanel historyList;

        public HistoryForm(GUI gui, Data data)
        {
            this.data = data;
            this.gui = gui;
            InitializeComponent();
            this.Text = "History";
            this.BackColor = Color.DarkGray;
            this.Size = new Size(500, 500);
            
            int formwidth = this.Width;
            int formheight = this.Height;

            initial = new FlowLayoutPanel();
            initial.Size = new Size(formwidth - 19, formheight - 9);

            historyList = new FlowLayoutPanel();
            historyList.Size = new Size(formwidth - 19, formheight - 39);
            historyList.AutoScroll = true;
            
            //historyList.AutoScroll = true;
            this.Controls.Add(initial);
            Label loading = new Label();
            loading.Size = new Size(200, 20);
            loading.Text = "Loading, please wait....";
            initial.Controls.Add(loading);
            initial.Refresh();
            BackgroundWorker populateWorker = new BackgroundWorker();
            populateWorker.DoWork += new DoWorkEventHandler(PopulateHistoryList);
            populateWorker.RunWorkerAsync();
        }

        internal void PopulateHistoryList(object sender, DoWorkEventArgs e)
        {
            History temp = (History)this.data.history.Clone();

            List<HistoryEntry> sortedList = temp.HistoryList.OrderBy(o => o.datetime).ToList();// In datetime order. 
            sortedList.Reverse(); // Reverse so the most recent is at the top of the GUI. 

                foreach (HistoryEntry entryObject in sortedList)
                {
                    FlowLayoutPanel entry = new FlowLayoutPanel(); // Use for each entry. 
                    entry.AutoSize = true;
                    TextBox date = new TextBox();
                    date.Size = new Size(150, 20);
                    date.BackColor = Color.Black;
                    date.ForeColor = Color.Green;
                    date.Text = entryObject.datetime.ToString(); // datetime is the dictionary key
                    TextBox url = new TextBox();
                    url.Size = new Size(150, 20);
                    url.BackColor = Color.Black;
                    url.ForeColor = Color.Green;
                    url.Text = entryObject.url; // url is the dictionary value
                    Button visit = new Button();
                    visit.Text = "Visit!";
                    visit.Name = url.Text;
                    visit.Click += new EventHandler(this.VisitNow);
                    entry.Controls.Add(date);
                    entry.Controls.Add(url);
                    entry.Controls.Add(visit);

                if (this.historyList.InvokeRequired)
                {
                    this.historyList.Invoke((MethodInvoker)(() =>
                    {
                        
                    }));
                }
                else
                {
                    historyList.Controls.Add(entry);
                }
                
            }
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)(() =>
                {
                    this.Controls.RemoveAt(0); // Remove loading sign. 
                    this.Controls.Add(historyList);
                    
                    historyList.Refresh();
                }));
            }
            else
            {
                this.Controls.RemoveAt(0); // Remove loading sign. 
                this.Controls.Add(historyList);

                historyList.Refresh();
            }
            
        }

        private void VisitNow(object sender, EventArgs e)
        {
            Button thesender = (Button)sender;

            if(gui.urlBox.InvokeRequired)
            {
                gui.urlBox.Invoke((MethodInvoker)(() =>
                {
                    gui.urlBox.Text = thesender.Name;
                }));
                
            }
            else
            {
                gui.urlBox.Text = thesender.Name;
            }
            
            if(gui.goButton.InvokeRequired)
            {
                gui.goButton.Invoke((MethodInvoker)(() =>
                {
                    gui.goButton.PerformClick();
                }));
                
            }
            else
            {
                gui.goButton.PerformClick();
            }
            
            this.Close();
            this.Dispose();
        }
    }
}
