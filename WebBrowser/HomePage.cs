﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBrowser
{
    [Serializable]
    public class HomePage
    {
        
        public string URL { get; set; }
    }
}
