﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace WebBrowser
{
    class Program
    {
        public Program()
        {
            
        }
        static void Main(string[] args)
        {
            Start.Initialise();
            Start.gui.FormClosing += new FormClosingEventHandler(CloseProgram); // Want to serialize classes when application being closed.
            Application.Run(Start.gui); // Run the Windows Form. 
        }

        private static void CloseProgram(object sender, FormClosingEventArgs e)
        {
            Start.DoSerialisation();
            Application.Exit();
            Environment.Exit(0);
        }
    }

    internal static class Start
    {
        internal static Data data; 
        internal static GUI gui;

        internal static void Initialise()
        {
            data = new Data(); // Instance of the Data holding class. 
            gui = new GUI(data); // Instance of the Windows Form. 
            Controller controller = new Controller(gui, data);
        }

        internal static void DoSerialisation()
        {
            data.DoSerialisation(data.homePage, "home.xml");
            data.DoSerialisation(data.history, "history.xml");
            data.DoSerialisation(data.favourites, "favourites.xml");
        }

    }
}
