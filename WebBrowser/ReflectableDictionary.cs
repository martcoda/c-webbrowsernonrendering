﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBrowser
{
    public struct StringsKeyValuePair
    {
        public string Key;
        public string Value;
    }
    public class ReflectableDictionary : IEnumerable<StringsKeyValuePair>
    {
        List<StringsKeyValuePair> theEntries = new List<StringsKeyValuePair>();
        public IEnumerator<StringsKeyValuePair> GetEnumerator()
        {

            foreach (StringsKeyValuePair skvp in theEntries)
            {
                yield return skvp; // Yield remembers where the iteration got to last time. 
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        /*public void Add(object o)
        {
            this.Options.Add(o as EffectOption); //you may want to extend the code to check that this cast can be made,
                                                 //and throw an appropriate error (otherwise it'll add null to your list)
        }*/

        public void Add(StringsKeyValuePair skvp)
        {
            this.theEntries.Add(skvp);
        }

        public bool ContainsKey(string key)
        {
            bool returner = false;
            foreach(StringsKeyValuePair svkp in this.theEntries)
            {
                if(svkp.Key == key)
                {
                    returner = true;
                    return returner;
                }
            }
            return returner;
        }

        public bool DeleteByKey(String key)
        {
            for(int x = 0; x < this.theEntries.Count; x++)
            {
                if(this.theEntries.ElementAt(x).Key == key)
                {
                    theEntries.RemoveAt(x);
                    return true;
                }
            }
            return false;
        }

        public void Clear()
        {
            this.theEntries.Clear();
        }


    }
}
