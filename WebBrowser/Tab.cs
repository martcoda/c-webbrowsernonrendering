﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Net;
using System.IO;
using System.Threading;
using System.Xml.Linq;
using System.Xml;

namespace WebBrowser
{
    class Tab
    {
        private Thread thread;

        

        private TabHistoryEntry currentTabHistory;

        private string URL = "";
        private string HTML;
        private FlowLayoutPanel tab;
        private string id;

        internal FlowLayoutPanel newPanel;
        internal Label emoticonLabel;
        internal TextBox newTextBox;
        internal Label closeLabel;

        private Data data;

        public TabHistoryEntry CurrentTabHistory
        {
            get { return this.currentTabHistory; }
            set { this.currentTabHistory = value; /*MessageBox.Show("updated current tabhistoryitem");*/ }
        }


        public string TabHTML
        {
            get { return this.HTML; }
            set { this.HTML = value; }
        }
        public string TabURL
        {
            get { return this.URL; }
            set
            {
                this.URL = value;
                if (this.newTextBox.InvokeRequired)
                {
                    this.newTextBox.Invoke((MethodInvoker)(() =>
                    {
                        this.newTextBox.Text = value;
                    }));
                }
                else
                {
                    this.newTextBox.Text = value;
                }
            }
        }

        public FlowLayoutPanel TabVar
        {
            get { return this.tab; }
            set { this.tab = value; }
        }

        public Tab(string name, Data data)
        {
            this.id = name;
            this.data = data;
            initialiseTab();
        }

        public string ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        // Methods for the thread. 
        public void Start() { thread.Start(); }
        public void Join() { try { thread.Join(); } catch (ThreadStateException tse) { Console.WriteLine("Tab thread exception when joining:" + tse.Message); } }
        public bool IsAlive { get { return thread.IsAlive; } }

        public void initialiseTab()
        {
             
            thread = new Thread(new ThreadStart(this.CreateTabItems));
            this.Start();
            this.Join();
        }
        private void CreateTabItems()
        {
            newPanel = new FlowLayoutPanel();
            newPanel.Name = this.ID;
            newPanel.Size = new System.Drawing.Size(190, 30);

           
            // This emoticon is not very happy to begin with, but later changes once a webpage has been downloaded. 
            emoticonLabel = new Label();
            emoticonLabel.Name = this.ID;
            emoticonLabel.Image = Properties.Resources.loading;
            emoticonLabel.Size = new Size(20, 20);

            newTextBox = new TextBox();
            newTextBox.Name = this.ID;

            newTextBox.Dock = DockStyle.Left;
            newTextBox.Text = this.TabURL;//
            newTextBox.Size = new Size(125, 55);

            newTextBox.BackColor = Color.Black;
            newTextBox.ForeColor = Color.Green;
            newTextBox.ReadOnly = true;
            newTextBox.Cursor = Cursors.Hand;

            Bitmap image = new Bitmap(Properties.Resources.close);
            Bitmap sizedImage = new Bitmap(image, 15, 15);
            closeLabel = new Label();
            closeLabel.Name = this.ID;

            closeLabel.Image = sizedImage;
            closeLabel.Size = new Size(20, 20);
            closeLabel.Cursor = Cursors.Hand;

            newPanel.Controls.Add(emoticonLabel);
            newPanel.Controls.Add(newTextBox);
            newPanel.Controls.Add(closeLabel);

            newPanel.BorderStyle = BorderStyle.FixedSingle;

            this.tab = newPanel;
        }

        internal void GetWebContent()
        {
            thread = new Thread(new ThreadStart(this.RunThread));
            this.Start();
            this.Join();
        }
        internal void RunThread()
        {

            // First of all check the cache. 
            if(this.data.CacheContainsCheck(this.URL))
            {
                
                // If the cache for this URL is not timed out, then retrieve the cache
                if (this.data.CacheNotTimedOut(this.URL))
                {
                    this.HTML = this.data.CacheGet(this.URL);
                }
                // Otherwise do a new http request to refresh the HTML and update the cache. 
                else
                {
                    this.HttpRequest();
                }
            }
            // If cache doesn't exist for this URL then make the http request, which will add to cache. 
            else
            {
                this.HttpRequest();
            }


           

            // The point of this emoticon is to show that the web content has been retrieved. It's a good way of seeing when a webpage has been downloaded. 
            if (this.emoticonLabel.InvokeRequired)
            {
                this.emoticonLabel.Invoke((MethodInvoker)(() =>
                {
                    this.emoticonLabel.Image = Properties.Resources.loaded;
                    this.emoticonLabel.Refresh();
                }));
            }
            else
            {
                this.emoticonLabel.Image = Properties.Resources.loaded;
                this.emoticonLabel.Refresh();
            }
        }

        internal void HttpRequest()
        {
            // If cache not valid then proceed to make a new request. 
            //From here https://msdn.microsoft.com/en-us/library/456dfw4f%28v=vs.110%29.aspx 
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this.URL);

                Console.WriteLine("made a request from " + this.ID);
                try
                {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    int responseCode = (int)response.StatusCode;
                    if (responseCode == 200)
                    {
                        System.IO.Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        string responseFromServer = reader.ReadToEnd();
                        //Console.WriteLine("finished receiving website from " + this.ID);



                        //MessageBox.Show("just finished reetrieving content");
                        // Display the content.
                        //Console.WriteLine(responseFromServer);

                        XmlDocument xdoc = new XmlDocument();
                        try
                        {
                            xdoc.Load(responseFromServer);
                            StringWriter stringWriter = new StringWriter();
                            XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);

                            xdoc.WriteTo(xmlTextWriter);

                            this.HTML = stringWriter.ToString();
                            this.data.CacheAdd(this.URL, this.HTML);

                            Console.WriteLine("used to xmldoc to load html");
                        }
                        catch (Exception ae)
                        {
                            if (ae is UriFormatException)
                            {
                                Console.WriteLine("URI was too long for xmldoc, will just pass HTML as string");
                            }
                            else if (ae is ArgumentException)
                            {
                                Console.WriteLine("Unable to load HTML into XmlDocument due to invalid characters, will just pass as string instead");

                            }
                            this.HTML = responseFromServer;
                            this.data.CacheAdd(this.URL, this.HTML);
                        }
                        reader.Close(); // Clean up. 
                        response.Close(); // Clean up. 
                    }
                }
                catch (System.Net.WebException we)
                {
                    Console.WriteLine("got an error with http request");
                    HttpWebResponse response = (HttpWebResponse)we.Response;
                    try
                    {
                        Console.WriteLine("server response is " + (int)response.StatusCode);

                        int responseCode = (int)response.StatusCode;
                        switch (responseCode)
                        {
                            case 400:
                                this.HTML = "400 Bad Request";
                                break;

                            case 403:
                                this.HTML = "403 Forbidden";
                                break;

                            case 404:
                                this.HTML = "404 Not Found";
                                break;
                            default:
                                this.HTML = "Encountered an unexpected problem... try turning off and on again...";
                                break;
                        }

                        // Clean up the streams and the response.

                        response.Close();
                    }
                    catch (NullReferenceException nre)
                    {
                        Console.WriteLine("null reference exception, i think invalid web url"+nre.InnerException);
                        this.HTML = "Invalid URL";
                    }


                }
            }

            catch (Exception iu)
            {
                Console.WriteLine(iu.Message);
            }
        }
    }
}
