﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBrowser
{
    class TabHistoryEntry
    {
        private HistoryEntry ownEntry = new HistoryEntry();

        private TabHistoryEntry previousEntry;
        private TabHistoryEntry nextEntry;

        public HistoryEntry OwnEntry
        {
            get { return this.ownEntry; }
        }


        public TabHistoryEntry(DateTime dt, String url)
        {
            ownEntry.datetime = dt;
            ownEntry.url = url;
        }

        internal TabHistoryEntry GetNext()
        {
            return this.nextEntry;
        }

        internal TabHistoryEntry GetPrevious()
        {
                return this.previousEntry;
        }

        internal void SetPrevious(TabHistoryEntry entry)
        {
            if (this.previousEntry == null)
            {
                this.previousEntry = entry;
            }
        }


        internal void SetNextOrLast(TabHistoryEntry entry)
        {
            if(this.nextEntry == null)
            {
                this.nextEntry = entry;
                this.nextEntry.SetPrevious(this);
            }
            else
            {
                this.GetNext().SetNextOrLast(entry);
            }
        }

        internal TabHistoryEntry GetLast()
        {
            if(this.nextEntry == null)
            {
                return this;
            }
            else
            {
                return this.nextEntry.GetLast();
            }
        }

    }
}
